# Insertion de données issues de Scrutari dans le site socioeco.org

Le site socioeco.org utilise une instance de Scrutari qui ne traite pas uniquement le contenu de socioeco.org mais également celui d'autres sites de l'ESS.

Cette application permet de valoriser ce contenu extérieur en l'affichant dans les pages web des sites concernés. Par exemple, la page de l'auteur Jean-Louis Laville sur le site socioeco.org affiche les liens des articles de son propre site : http://www.socioeco.org/bdf_auteur-196_fr.html

Ces liens sont insérés à l'aide d'une balise <iframe> qui appelle l'application (elle-même accessible via http://apps.socioeco.org/widget/).

