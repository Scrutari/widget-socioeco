<?php
/***************************************************************
* Socioeco.org Scrutari Widget - Web resources display based on Scrutari search Engine
* http://www.scrutari.net/ - http://www.socioeco.org
*
* Copyright (c) 2015-2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

$GLOBALS['scrutari'] = array();
$GLOBALS['scrutari']['engine'] = array();
initQuery();
initEngineOptions();
initLangui();
initOrigin();
initFicheFields();


function initQuery() {
    if(isset($_REQUEST['q'])) {
        $q = trim($_REQUEST['q']);
        if (strlen($q) > 0) {
            $GLOBALS['scrutari']['query'] = $q;
            $GLOBALS['scrutari']['type'] = 'query';
        }
    } else if(isset($_REQUEST['motcle'])) {
        $motcle = trim($_REQUEST['motcle']);
        if (strlen($motcle) > 0) {
            $GLOBALS['scrutari']['motcle'] = $motcle;
            $GLOBALS['scrutari']['type'] = 'motcle';
        }
    } else if(isset($_REQUEST['base'])) {
        $base = trim($_REQUEST['base']);
        if (strlen($base) > 0) {
            $GLOBALS['scrutari']['base'] = $base;
            $GLOBALS['scrutari']['type'] = 'base';
        }
    } else if(isset($_REQUEST['axe'])) {
        $axe = trim($_REQUEST['axe']);
        if (strlen($axe) > 0) {
            $GLOBALS['scrutari']['axe'] = $axe;
            $GLOBALS['scrutari']['type'] = 'axe';
        }
    } else if(isset($_REQUEST['dossier'])) {
        $dossier = trim($_REQUEST['dossier']);
        if (strlen($dossier) > 0) {
            $GLOBALS['scrutari']['dossier'] = $dossier;
            $GLOBALS['scrutari']['type'] = 'dossier';
        }
    }
}

function initLangui() {
    $langui = 'fr';
    $l10n = 'fr';
    //initLoc('fr');
    if (isset($_REQUEST['lang'])) {
        $languiParam = $_REQUEST['lang'];
        if (preg_match('/^[-a-zA-Z_]+$/', $languiParam)) {
            $langui = $languiParam;
            if ($langui != 'fr') {
                /*initLoc($langui);*/
                if (file_exists("scrutarijs/js/l10n/".$langui.".js")) {
                    $l10n = $langui;
                }
            }
        }
    } else if (isset($_REQUEST['langui'])) {
        $languiParam = $_REQUEST['langui'];
        if (preg_match('/^[-a-zA-Z_]+$/', $languiParam)) {
            $langui = $languiParam;
            if ($langui != 'fr') {
                /*initLoc($langui);*/
                if (file_exists("scrutarijs/js/l10n/".$langui.".js")) {
                    $l10n = $langui;
                }
            }
        }
    }
    $GLOBALS['scrutari']['langui'] = $langui;
    $GLOBALS['scrutari']['l10n'] = $l10n;
}

function initEngineOptions() {
    $GLOBALS['scrutari']['engine']['name'] = "socioeco";
    $GLOBALS['scrutari']['engine']['url'] = "http://sct1.scrutari.net/sct/socioeco/";
}

function initOrigin() {
    $GLOBALS['scrutari']['origin'] = 'widget';
}

function initFicheFields() {
    $fields = "codecorpus,mtitre,msoustitre,mcomplements,annee,href";
    $withAttrs = true;
    if(isset($_REQUEST['attrs'])) {
        $attrs = trim($_REQUEST['attrs']);
        if ($attrs == 'false') {
            $withAttrs = false;
        }
    }
    if ($withAttrs) {
        $fields .= ",mattrs_all";
    }
    $GLOBALS['scrutari']['fields'] = $fields;
}

?>


<!DOCTYPE html>
<html lang="<?php echo $GLOBALS['scrutari']['langui'];?>">
<head>
<title>Socioeco.org</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="static/icon.png" type="image/png" rel="icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" href="bootstrap/3.3.7/css/bootstrap.min.css">
<script src="bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="widget.css">
<script src="scrutari.js"></script>
<script src="widget.js"></script>
<script src="l10n/<?php echo $GLOBALS['scrutari']['l10n']; ?>.js"></script>
<style>
#ficheDisplayBlock div.scrutari-fiche-Block {
    border-left-style: none;
}
</style>

<script>
    $(function () {
        var scrutariConfig = new Scrutari.Config("<?php echo $GLOBALS['scrutari']['engine']['name'];?>","<?php echo $GLOBALS['scrutari']['engine']['url'];?>", "<?php echo $GLOBALS['scrutari']['langui'];?>", "<?php echo $GLOBALS['scrutari']['origin'];?>");
        scrutariConfig.setFicheFields("<?php echo addslashes($GLOBALS['scrutari']['fields']);?>");
        scrutariConfig.setPlageLength(5);
        var widget = new Widget(scrutariConfig, Widget.l10n, "<?php echo addslashes($GLOBALS['scrutari']['type']);?>");
        <?php if ($GLOBALS['scrutari']['type'] == 'motcle') { ?>
        widget.setMotcle("<?php echo addslashes($GLOBALS['scrutari']['motcle']);?>");
        <?php } else if ($GLOBALS['scrutari']['type'] == 'query') { ?>
        widget.setInitialQuery("<?php echo addslashes($GLOBALS['scrutari']['query']);?>");
        <?php } else if ($GLOBALS['scrutari']['type'] == 'base') { ?>
        widget.setBaseURI("<?php echo addslashes($GLOBALS['scrutari']['base']);?>");
        <?php } else if ($GLOBALS['scrutari']['type'] == 'axe') { ?>
        widget.setAxe("<?php echo addslashes($GLOBALS['scrutari']['axe']);?>");
         <?php } else if ($GLOBALS['scrutari']['type'] == 'dossier') { ?>
        widget.setDossier("<?php echo addslashes($GLOBALS['scrutari']['dossier']);?>");
        <?php } ?>
        var _scrutariMetaCallback = function (scrutariMeta) {
           widget.init(scrutariMeta);
        };
        Scrutari.Meta.load(_scrutariMetaCallback, scrutariConfig);
    });
</script>

</head>
<body>
<div class="container" id="mainContainer">
    <div class="row">
        <div class="col-sm-7 col-sm-push-2" id="ficheDisplayBlock">
        </div>
    </div>
</div>
</body>
</html>